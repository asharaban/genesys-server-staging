# TODO Historic records #

- Copy /explore to /archive to allow access to historic entries?
- Use archives to improve resolver service

# TODO KPI #

- Figure out how to trigger execution runs in a distributed environment
- Need a GUI to display observations
- Can we ignore observations that have the same result as previous execution run?
- How frequently do we trigger the runs?

# TODO PDCI #

- Use DS to hold results of calculation, similar to worldclim DS
- Re-calculation needs to happen after any update to Accession or AccessionRelated (same as ES update)
- PDCI dataset is downloadable as Excel: this works on institute level, but not for selected accessions
- WorldClim dataset is downloadable as Excel (for selected accessions)

# TODO Accession Lists

- Make new list from applied filter (limit list size to X)
- FIXME Blank list does not load and cannot be removed
- Listing accessions by list (in Browse) needs a better completer
- A decent user interface

## Set operations ##

- Add set operations: union (A ∪ B), intersection (A ∩ B),
  complement (A ∖ B, B ∖ A), symmetric difference (A △ B)
- Temporary (session scoped) sets? -- similar to the existing `SelectionBean`? 
  Bad idea, big lists needs to sync across the cluster.
- Should consider making the accession lists a new functionality: create list from
  selection, but not the other way around. Selection should be a small list, while
  Accession lists may contain 1,000s of items.
  
## Filters and Accession sets ##

Rename "Accession Lists" to "Accession Sets". When the set is created from a filtered
list of accessions, the filter should be stored with set metadata. This would potentially
allow for updating the set or identifying missing/new elements.

 