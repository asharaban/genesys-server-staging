<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title><spring:message code="project.page.profile.title" arguments="${project.name}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		${project.name}
		<small>
			<c:out value="${project.code}" />
		</small>
	</h1>

	<form role="form" class="form-horizontal" action="<c:url value="/project/update-project" />" method="post">
		<input type="hidden" name="id" value="${project.id}" />
		<input type="hidden" name="version" value="${project.version}" />
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

		<div class="form-group">
			<label class="col-lg-3 control-label">
				<spring:message code="project.code" />
			</label>
			<div class="controls col-lg-9">
				<input type="text" name="code" class="form-control required" value="${project.code}" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label">
				<spring:message code="project.name" />
			</label>
			<div class="controls col-lg-9">
				<input type="text" name="name" class="form-control required" value="${project.name}" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">
				<spring:message code="project.url" />
			</label>
			<div class="controls col-lg-9">
				<input type="text" name="url" class="form-control required" placeholder="https://" value="${project.url}" />
			</div>
		</div>

		<div class="form-group">
			<label for="blurp-body" class="col-lg-12 control-label">
				<spring:message code="blurp.blurp-body" />
			</label>
			<div class="controls col-lg-12">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
			</div>
		</div>

		<div class="form-group">
			<label for="project-summary" class="col-lg-12 control-label">
				<spring:message code="project.summary" />
			</label>
			<div class="controls col-lg-12">
				<textarea id="project-summary" name="summary" class="span9 required html-editor">
					<c:out value="${blurp.summary}" />
				</textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">
				<spring:message code="project.accessionLists" />
			</label>
			<div class="controls col-lg-9">
				<c:forEach items="${project.accessionLists}" var="accessionList">
					<div>
						<input type="text" name="accessionLists" class="form-control" value="${accessionList}" />
					</div>
				</c:forEach>
				<div>
					<input type="text" name="accessionLists" class="form-control" value="" placeholder="u-u-i-d" />
				</div>
			</div>
		</div>

		<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
		<a href="<c:url value="/project/${project.code}" />" class="btn btn-default">
			<spring:message code="cancel" />
		</a>

	</form>


	<content tag="javascript"> <script type="text/javascript">
    <local:tinyMCE selector=".html-editor" />
  </script> </content>

</body>
</html>
