#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=No autorizado
http-error.401.text=Se requiere autenticación.
http-error.403=Acceso denegado
http-error.403.text=No tiene permiso para acceder al recurso.
http-error.404=No encontrado
http-error.404.text=El recurso solicitado no se ha encontrado, pero podría volver a estar disponible en el futuro.
http-error.500=Error interno del servidor
http-error.500.text=Se ha encontrado una situación inesperada. No hay otro mensaje específico más adecuado.
http-error.503=Servicio no disponible
http-error.503.text=El servidor no está disponible en estos momentos (por sobrecarga o mantenimiento).


# Login
login.username=Nombre de usuario
login.password=Contraseña
login.invalid-credentials=Acreditación inválida.
login.remember-me=Recordar mis datos de usuario
login.login-button=Iniciar sesión
login.register-now=Crear una cuenta
logout=Cerrar sesión
login.forgot-password=He olvidado mi contraseña
login.with-google-plus=Iniciar sesión con Google+

# Registration
registration.page.title=Crear una cuenta de usuario
registration.title=Crear su cuenta
registration.invalid-credentials=Acreditación inválida.
registration.user-exists=Nombre de usuario ya utilizado.
registration.email=Correo electrónico
registration.password=Contraseña
registration.confirm-password=Repetir contraseña
registration.full-name=Nombre completo
registration.create-account=Crear cuenta
captcha.text=Texto de la imagen


id=Identificación

name=Nombre
description=Descripción
actions=Acciones
add=Agregar
edit=Editar
save=Guardar
create=Crear
cancel=Cancelar
delete=Borrar

jump-to-top=Volver arriba

pagination.next-page=Siguiente >
pagination.previous-page=< Anterior


# Language
locale.language.change=Cambio de ubicación
i18n.content-not-translated=El contenido de esta página no está disponible en su idioma. Póngase en contacto con nosotros si puede ayudarnos a traducirlo.

data.error.404=La información solicitada no se ha encontrado en el sistema.
page.rendertime=Esta página se procesó en {0} ms.

footer.copyright-statement=&copy; 2013 - 2015 Proveedores de datos y el Crop Trust

menu.home=Página de inicio
menu.browse=Navegar
menu.datasets=Datos de C&E
menu.descriptors=Descriptores
menu.countries=Países
menu.institutes=Instituciones
menu.my-list=Mi lista
menu.about=Acerca de Genesys
menu.contact=Contacte con nosotros
menu.disclaimer=Cláusula de protección
menu.feedback=Comentarios
menu.help=Ayuda
menu.terms=Términos y condiciones de uso
menu.copying=Política de derechos de autor
menu.privacy=Política de privacidad
menu.newsletter=Boletín de Genesys
menu.join-the-community=Unirse a la comunidad de Genesys
menu.faq=Preguntas frecuentes
menu.news=Noticias
page.news.title=Noticias
page.news.intro=Lo último de toda la comunidad Genesys, desde detalles sobre los miembros más recientes hasta actualizaciones sobre accesiones que se están conservando en todo el mundo.

# Extra content
menu.what-is-genesys=¿Qué es Genesys?
menu.who-uses-genesys=¿Quién utiliza Genesys?
menu.how-to-use-genesys=¿Cómo utilizar Genesys?
menu.history-of-genesys=Historia de Genesys
menu.about-genesys-data=Acerca de los datos de Genesys



page.home.title=Recursos fitogenéticos de Genesys

user.pulldown.administration=Administración
user.pulldown.users=Lista de usuarios
user.pulldown.logout=Cerrar sesión
user.pulldown.profile=Mi perfil
user.pulldown.oauth-clients=Clientes OAuth
user.pulldown.teams=Equipos
user.pulldown.manage-content=Gestionar el contenido

user.pulldown.heading={0}
user.create-new-account=Crear una cuenta de usuario
user.full-name=Nombre completo
user.email=Dirección de correo electrónico
user.account-status=Estado de cuenta
user.account-disabled=Cuenta deshabilitada
user.account-locked-until=Cuenta bloqueada hasta
user.roles=Papeles del usuario
userprofile.page.title=Perfil de usuario
userprofile.page.intro=Como comunidad, Genesys se basa en sus usuarios para tener éxito. Tanto si es usted un investigador individual como una institución mayor, puede actualizar su perfil aquí para mostrar sus intereses y los datos de su colección.
userprofile.update.title=Actualice su perfil

user.page.list.title=Cuentas de usuarios registradas

crop.croplist=Lista de cultivos
crop.all-crops=Todos los cultivos
crop.taxonomy-rules=Reglas taxonómicas
crop.view-descriptors=Ver descriptores de cultivos...
crop.page.edit.title=Editar {0}
crop.summary=Resumen (metadatos HTML)

activity.recent-activity=Actividad reciente

country.page.profile.title=Perfil del país\: {0}
country.page.list.title=Lista de países
country.page.list.intro=Haciendo clic en un nombre de país, puede encontrar instituciones registradas en el país así como estadísticas sobre recursos genéticos de plantas en el país en conjunto.
country.page.not-current=Esta es una entrada histórica
country.page.faoInstitutes={0} instituciones registradas en WIEWS
country.stat.countByLocation={0} accesiones conservadas en instituciones de este país
country.stat.countByOrigin={0} accesiones registradas en Genesys provienen de este país.
country.statistics=Estadísticas del país
country.accessions.from=Accesiones recogidas en {0}
country.more-information=Más información\:
country.replaced-by=El código de país se ha reemplazado por\: {0}
country.is-itpgrfa-contractingParty={0} es parte del Tratado internacional sobre los recursos fitogenéticos para la alimentación y la agricultura (TIRFAA).
select-country=Seleccionar país

project.page.list.title=Proyectos
project.page.profile.title={0}
project.code=Código del proyecto
project.name=Nombre del proyecto
project.url=Página web del proyecto
project.summary=Resumen (metadatos HTML)
project.accessionLists=Lista de accesión


faoInstitutes.page.list.title=Instituciones miembros de WIEWS
faoInstitutes.page.profile.title=WIEWS {0} - Sistema Mundial de Información y Alerta sobre los Recursos Fitogenéticos para la Alimentación y la Agricultura
faoInstitutes.stat.accessionCount=Accesiones en Genesys\:
faoInstitutes.stat.datasetCount=Grupos de información adicionales en Genesys\:
faoInstitute.stat-by-crop=Cultivos más representados
faoInstitute.stat-by-genus=Géneros más representados
faoInstitute.stat-by-species=Especies más representadas
faoInstitute.accessionCount={0} accesiones
faoInstitute.statistics=Estadísticas institucionales
faoInstitutes.page.data.title=Accesiones en {0}
faoInstitute.accessions.at=Accesiones en {0}
faoInstitutes.viewAll=Ver todas las instituciones registradas
faoInstitutes.viewActiveOnly=Ver instituciones con accesiones en Genesys
faoInstitute.no-accessions-registered=Póngase en contacto con nosotros si puede proveer información desde esta institución.
faoInstitute.institute-not-current=Este registro se ha archivado.
faoInstitute.view-current-institute=Ir a {0}.
faoInstitute.country=País
faoInstitute.code=Código en WIEWS
faoInstitute.email=Correo electrónico de contacto
faoInstitute.acronym=Siglas
faoInstitute.url=Enlace web
faoInstitute.summary=Resumen (metadatos HTML)
faoInstitute.member-of-organizations-and-networks=Organizaciones y grupos de trabajo\:
faoInstitute.uniqueAcceNumbs.true=Todos los números de identificación de una accesión son únicos dentro de esta institución.
faoInstitute.uniqueAcceNumbs.false=El mismo número de accesión puede haberse utilizado en distintas colecciones de esta institución.
faoInstitute.requests.mailto=Dirección de correo electrónico para las solicitudes de germoplasma\:
faoInstitute.allow.requests=Permitir peticiones de material
faoInstitute.sgsv=Código SGSV
faoInstitute.data-title=Base de datos de información de accesiones mantenida en {0}
faoInstitute.overview-title=Resumen de información de accesiones mantenido en {0}
faoInstitute.datasets-title=Descripción y valoración de los datos aportados por {0}
faoInstitute.meta-description=Resumen de los recursos fitogenéticos mantenidos en colecciones en {0} ({1}) bancos de genes
faoInstitute.link-species-data=Ver información de {2} accesiones en {0} ({1})
faoInstitute.wiewsLink={0} detalles en el sitio web de FAO WIEWS

view.accessions=Ver accesiones
view.datasets=Ver conjuntos de datos
paged.pageOfPages=Página {0} de {1}
paged.totalElements={0} entradas

accessions.number={0} accesiones
accession.metadatas=Datos de caracterización y evaluación
accession.methods=Datos de caracterización y evaluación
unit-of-measure=Unidad de medida

ce.trait=Rasgo característico
ce.sameAs=Al igual que
ce.methods=Métodos
ce.method=Método
method.fieldName=Campo de la base de datos

accession.uuid=Identificador Universalmente Único
accession.accessionName=Número de accesión
accession.acceNumb=Número de accesión
accession.origin=País de origen
accession.orgCty=País de origen
accession.holdingInstitute=Institución poseedora
accession.institute.code=Institución poseedora
accession.holdingCountry=Ubicación
accession.institute.country.iso3=Ubicación
accession.taxonomy=Nombre científico
accession.taxonomy.sciName=Nombre científico
accession.genus=Género
accession.taxonomy.genus=Género
accession.species=Especie
accession.taxonomy.species=Especie
accession.subtaxa=Subtaxa
accession.taxonomy.subtaxa=Subtaxa
accession.crop=Nombre del cultivo
accession.crops=Nombre del cultivo
accession.otherNames=También conocido como
accession.inTrust=En "Trust"
accession.mlsStatus=Estado del Sistema Multilateral (MLS)
accession.duplSite=Institución de duplicado de seguridad
accession.inSvalbard=Duplicado de seguridad en Svalbard 
accession.inTrust.true=Esta accesión está sujeta al Artículo 15 del Tratado internacional sobre los recursos fitogenéticos para la alimentación y la agricultura.
accession.mlsStatus.true=Esta accesión se incluye en el Sistema multilateral del Tratado internacional sobre los recursos fitogenéticos para la alimentación y la agricultura.
accession.inSvalbard.true=Duplicado de seguridad en la Bóveda Global de Semillas de Svalbard.
accession.not-available-for-distribution=Accesión NO disponible para su distribución.
accession.this-is-a-historic-entry=Este es el registro histórico de una accesión. 
accession.historic=Entrada histórica
accession.available-for-distribution=La accesión está disponible para su distribución.
accession.elevation=Elevación
accession.geolocation=Ubicación geográfica (lat., long.)

accession.storage=Tipo de almacenamiento del germoplasma
accession.storage.=
accession.storage.10=Colección de semillas
accession.storage.11=Colección de semillas a corto plazo
accession.storage.12=Colección de semillas a mediano plazo
accession.storage.13=Colección de semillas a largo plazo
accession.storage.20=Colección de campo
accession.storage.30=Colección in vitro
accession.storage.40=Colección criopreservada
accession.storage.50=Colección de ADN
accession.storage.99=Otro

accession.breeding=Información del criador
accession.breederCode=Código del criador
accession.pedigree=Genealogía
accession.collecting=Información de la colección
accession.collecting.site=Localización del sitio de colección
accession.collecting.institute=Institución colectora
accession.collecting.number=Número de colección
accession.collecting.date=Fecha de colección de la muestra
accession.collecting.mission=Identificador de la campaña de recolección
accession.coll.collMissId=ID de la misión de colecta
accession.collecting.source=Colección/Origen de la adquisición

accession.collectingSource.=
accession.collectingSource.10=Hábitat silvestre
accession.collectingSource.11=Bosque o arboleda
accession.collectingSource.12=Matorral
accession.collectingSource.13=Pastizal
accession.collectingSource.14=Desierto o tundra
accession.collectingSource.15=Hábitat acuático
accession.collectingSource.20=Hábitat de cultivo o de campo
accession.collectingSource.21=Campo
accession.collectingSource.22=Huerto
accession.collectingSource.23=Huerto familiar (urbano, de la periferia o rural), jardín o patio trasero
accession.collectingSource.24=Terreno de barbecho
accession.collectingSource.25=Pastura
accession.collectingSource.26=Almacén de compras rural
accession.collectingSource.27=Residuos de cosecha
accession.collectingSource.28=Parque
accession.collectingSource.30=Mercado o tienda
accession.collectingSource.40=Instituto, Estación experimental, Organización de investigación, Banco de genes
accession.collectingSource.50=Empresa semillera
accession.collectingSource.60=Área ruderal o perturbada, hábitat de maleza
accession.collectingSource.61=Arcén
accession.collectingSource.62=Margen de campo
accession.collectingSource.99=Otro

accession.donor.institute=Institución donante
accession.donor.accessionNumber=Identificador del donante de la accesión
accession.geo=Información geográfica
accession.geo.datum=Datos de coordenadas
accession.geo.method=Método de georreferenciación
accession.geo.uncertainty=Incertidumbre de coordenadas
accession.geo.latitudeAndLongitude=Ubicación geográfica

accession.sampStat=Estado biológico de la accesión
accession.sampleStatus=Estado biológico de la accesión
accession.sampleStatus.=
accession.sampleStatus.100=Silvestre
accession.sampleStatus.110=Natural
accession.sampleStatus.120=Seminatural/Silvestre
accession.sampleStatus.130=Seminatural/cultivado
accession.sampleStatus.200=Maleza
accession.sampleStatus.300=Variedad cultivada tradicional/local
accession.sampleStatus.400=Germoplasma para cultivo/investigación
accession.sampleStatus.410=Línea de criadores
accession.sampleStatus.411=Población sintética
accession.sampleStatus.412=Híbrido
accession.sampleStatus.413=Stock genético fundador/población base
accession.sampleStatus.414=Línea consanguínea
accession.sampleStatus.415=Población segregante
accession.sampleStatus.416=Selección clónica
accession.sampleStatus.420=Stock genético
accession.sampleStatus.421=Mutante
accession.sampleStatus.422=Reservas citogenéticas
accession.sampleStatus.423=Otros stocks genéticos
accession.sampleStatus.500=Variedad cultivada avanzada/mejorada
accession.sampleStatus.600=OMG (Organismo modificado genéticamente)
accession.sampleStatus.999=Otro

accession.availability=Disponibilidad para distribución
accession.aliasType.ACCENAME=Nombre de accesión
accession.aliasType.DONORNUMB=Identificador del donante de la accesión
accession.aliasType.BREDNUMB=Nombre asignado por el criador
accession.aliasType.COLLNUMB=Número de coleción
accession.aliasType.OTHERNUMB=Otros nombres
accession.aliasType.DATABASEID=(Identificador de base de datos)
accession.aliasType.LOCALNAME=(Nombre local)

accession.availability.=Desconocido
accession.availability.true=Disponible
accession.availability.false=No disponible

accession.historic.true=Historial
accession.historic.false=Activo
accession.acceUrl=URL de accesiones adicionales
accession.scientificName=Nombre científico

accession.page.profile.title=Perfil de la accesión\: {0}
accession.page.resolve.title=Se han encontrado múltiples accesiones
accession.resolve=Se han encontrado múltiples accesiones con el nombre ''{0}'' en Genesys. Seleccione una de la lista.
accession.page.data.title=Navegador de accesiones
accession.page.data.intro=Explore los datos de accesión con Genesys. Para afinar su búsqueda, añada filtros tal como país de origen, cultivo, especie, o la latitud y longitud del sitio de colección.
accession.taxonomy-at-institute=Ver {0} en {1}

accession.svalbard-data=Información de duplicados en la Bóveda Global de Semillas de Svalbard
accession.svalbard-data.taxonomy=Nombre científico indicado
accession.svalbard-data.depositDate=Fecha de depósito
accession.svalbard-data.boxNumber=Número de caja
accession.svalbard-data.quantity=Cantidad
accession.remarks=Observaciones

taxonomy.genus=Género
taxonomy.species=Especie
taxonomy.taxonName=Nombre científico
taxonomy-list=Lista taxonómica

selection.checkbox=Haga clic para añadir o eliminar la accesión de su lista
selection.page.title=Accesiones seleccionadas
selection.page.intro=A medida que explora los millones de accesiones que hay en Genesys, puede crear su propia lista para mantener un seguimiento de los resultados de su búsqueda. Sus selecciones estás guardadas aquí, de forma que puede volver a ellas en todo momento.
selection.add=Agregar {0} a la lista
selection.remove=Eliminar {0} de la lista
selection.clear=Borrar la lista
selection.reload-list=Volver a cargar la lista
selection.map=Mostrar el mapa de las accesiones
selection.send-request=Enviar solicitud de germoplasma
selection.empty-list-warning=No ha agregado accesiones a la lista.
selection.add-many=Añadir múltiples accesiones
selection.add-many.accessionIds=Lista de número de accesiones (ACCENUMB) registradas en Genesys, separadas por una coma, punto y coma o en una nueva línea.
selection.add-many.instCode=Código del instituto poseedor
selection.add-many.button=Agregar a la lista seleccionada

savedmaps=Recordar mapa actual
savedmaps.list=Lista de mapas
savedmaps.save=Recordar mapa
taxonomy.subtaxa=Subtaxa

filter.enter.title=Introduzca el título de filtro
filters.page.title=Filtros de datos
filters.view=Filtros actuales
filter.filters-applied=Ha aplicado filtros.
filter.filters-not-applied=Puede filtrar los datos.
filters.data-is-filtered=Información filtrada.
filters.toggle-filters=Filtros
filter.taxonomy=Nombre científico
filter.art15=La accesión se recoge en el Artículo 15 del TIRFAA.
filter.acceNumb=Número de accesión
filter.seqNo=Número secuencial detectado
filter.alias=Nombre de accesión
filter.crops=Nombre del cultivo
filter.orgCty.iso3=País de origen
filter.institute.code=Nombre de la institución poseedora
filter.institute.country.iso3=País de la institución poseedora
filter.sampStat=Estatus biológico de la accesión
filter.institute.code=Nombre de la institución poseedora
filter.geo.latitude=Latitud
filter.geo.longitude=Longitud
filter.geo.elevation=Elevación
filter.taxonomy.genus=Género
filter.taxonomy.species=Especie
filter.taxonomy.subtaxa=Subtaxa
filter.taxSpecies=Especies
filter.taxonomy.sciName=Nombre científico
filter.sgsv=Duplicado de seguridad en Svalbard
filter.mlsStatus=Estado de la accesión en el Sistema Multilateral (MLS) 
filter.available=Disponible para su distribución
filter.historic=Registro histórico
filter.donorCode=Institución donante
filter.duplSite=Sitio de la duplicación de seguridad
filter.download-dwca=Descargar ZIP
filter.download-mcpd=Descargar MCPD
filter.add=Afinar la búsqueda por categoría
filter.additional=Afinar la búsqueda por rasgo característico
filter.apply=Aplicar
filter.close=Cerrar
filter.remove=Eliminar filtro
filter.autocomplete-placeholder=Escriba más de 3 caracteres...
filter.coll.collMissId=ID de la misión de colecta
filter.storage=Tipo de almacenamiento del germoplasma
filter.string.equals=Igual
filter.string.like=Empieza con
filter.inverse=Excluyendo
filter.set-inverse=Excluir los valores seleccionados
filter.internal.message.like=Me gusta {0}
filter.internal.message.between=Entre {0}
filter.internal.message.and={0} y {0}
filter.internal.message.more=Más de {0}
filter.internal.message.less=Menos de {0}
filter.lists=Enumeradas en la lista de accesión

columns.add=Cambiar las columnas mostradas
columns.apply=Aplicar
columns.availableColumns=Seleccionar las columnas para mostrar y hacer clic en Aplicar
columns.latitudeAndLongitude=Latitud y longitud
columns.collectingMissionID=ID de la misión de colecta

columns.acceNumb=Número de accesión
columns.scientificName=Nombre científico
columns.orgCty=País de origen
columns.sampStat=Estado biológico de la accesión
columns.instCode=Institución poseedora

search.page.title=Buscador de texto completo
search.no-results=No se han encontrado coincidencias para su búsqueda.
search.input.placeholder=Buscar en Genesys...
search.search-query-missing=Introduzca su consulta de búsqueda.
search.search-query-failed=Lo sentimos, se produjo un error en la búsqueda {0}
search.button.label=Buscar
search.add-genesys-opensearch=Registrar la búsqueda de Genesys con su explorador

admin.page.title=Administración de Genesys 2
metadata.page.title=Grupos de información
metadata.page.intro=Navegue por los grupos de datos de caracterización y evaluación subidos a Genesys por investigadores y organizaciones de investigadores. Para cada uno puede descargar los datos en formato Excel o CVS.
metadata.page.view.title=Detalles del grupo de información
metadata.download-dwca=Descargar ZIP
page.login=Iniciar sesión

traits.page.title=Descriptores
trait-list=Descriptores


organization.page.list.title=Organizaciones y grupos de trabajo
organization.page.profile.title={0}
organization.slug=Acrónimo de la organización o grupo de trabajo
organization.title=Nombre completo
organization.summary=Resumen (metadatos HTML)
filter.institute.networks=Red


menu.report-an-issue=Notificar un problema
menu.scm=Código de origen
menu.translate=Traducir Genesys

article.edit-article=Editando el artículo
article.slug=Página de artículo (URL)
article.title=Título del artículo
article.body=Cuerpo del artículo
article.summary=Resumen (metadatos HTML)
article.post-to-transifex=Publicar en Transifex
article.remove-from-transifex=Eliminar de Transifex
article.fetch-from-transifex=Obtener traducciones
article.transifex-resource-updated=El recurso se ha actualizado con éxito en Transifex.
article.transifex-resource-removed=El recurso se ha eliminado con éxito de Transifex.
article.transifex-failed=Ha ocurrido un error mientras se intercambiaban los datos con Transifex
article.translations-updated=¡Recurso actualizado con éxito con datos traducidos\!
article.share=Compartir este artículo

user.accession.list.saved-updated=Se ha guardado correctamente su lista de accesiones.
user.accession.list.deleted=Se ha eliminado correctamente del servidor su lista de accesiones.
user.accession.list.create-update=Guardar
user.accession.list.delete=Eliminar
user.accession.list.title=Lista de accesión

activitypost=Publicación de actividad
activitypost.add-new-post=Agregar una nueva publicación
activitypost.post-title=Título de la publicación
activitypost.post-body=Cuerpo

blurp.admin-no-blurp-here=Sin descripción.
blurp.blurp-title=Título de la descripción
blurp.blurp-body=Contenidos de la descripción
blurp.update-blurp=Guardar descripción


oauth2.confirm-request=Confirmar acceso
oauth2.confirm-client=En virtud del presente escrito, usted, <b>{0}</b>, autoriza a acceder a <b>{1}</b> a sus recursos protegidos.
oauth2.button-approve=Sí, permitir acceso
oauth2.button-deny=No, denegar acceso

oauth2.authorization-code=Código de autorización
oauth2.authorization-code-instructions=Copiar el código de autorización\:

oauth2.access-denied=Acceso denegado
oauth2.access-denied-text=Ha denegado el acceso a sus recursos.

oauth-client.page.list.title=Clientes OAuth2 
oauth-client.page.profile.title=Cliente OAuth2\: {0}
oauth-client.active-tokens=Lista de muestras emitidas
client.details.title=Título del cliente
client.details.description=Descripción

maps.loading-map=Cargando mapa...
maps.view-map=Ver mapa
maps.accession-map=Mapa de las accesiones
maps.accession-map.intro=El mapa muestra el sitio de colección de accesiones georeferenciadas.
maps.baselayer.list=Cambiar la capa base del mapa

audit.createdBy=Creado por {0}
audit.lastModifiedBy=Última actualización por {0}

itpgrfa.page.list.title=Partes del TIRFAA

request.page.title=Solicitar material de las instituciones poseedoras
request.total-vs-available=De {0} accesiones listadas, {1} están disponibles para su distribución.
request.start-request=Solicitar germoplasma disponible
request.your-email=Su dirección de correo electrónico\:
request.accept-smta=Aceptación del protocolo SMTA/MTA\:
request.smta-will-accept=Acepto los términos y condiciones del protocolo SMTA/MTA
request.smta-will-not-accept=NO acepto los términos y condiciones del protocolo SMTA/MTA
request.smta-not-accepted=No ha aceptado los términos y condiciones del protocolo SMTA/MTA. Su solicitud no será procesada.
request.purpose=Especifique el uso del material\:
request.purpose.0=Otros (describa en el campo para notas)
request.purpose.1=Investigación para la alimentación y la agricultura
request.notes=Añada cualquier comentario o nota adicional\:
request.validate-request=Valide su solicitud
request.confirm-request=Confirmar la recepción de la solicitud
request.validation-key=Clave de validación\:
request.button-validate=Validar
validate.no-such-key=La clave de validación no es válida.

team.user-teams=Equipos de los usuarios
team.create-new-team=Crear un nuevo grupo
team.team-name=Nombre del grupo
team.leave-team=Abandonar el grupo
team.team-members=Miembros del grupo
team.page.profile.title=Grupo\: {0}
team.page.list.title=Todos los grupos
validate.email.key=Introduzca la clave
validate.email=Validación del correo electrónico
validate.email.invalid.key=Contraseña no válida

edit-acl=Permisos de edición
acl.page.permission-manager=Administrador de permisos
acl.sid=Identidad de seguridad
acl.owner=Propietario del objeto
acl.permission.1=Leer
acl.permission.2=Escribir
acl.permission.4=Crear
acl.permission.8=Eliminar
acl.permission.16=Administrar


ga.tracker-code=Código de rastreo GA

boolean.true=Sí
boolean.false=No
boolean.null=Desconocido
userprofile.password=Restablecer contraseña
userprofile.enter.email=Introduzca su correo electrónico
userprofile.enter.password=Introduzca su nueva contraseña
userprofile.email.send=Enviar correo electrónico

verification.invalid-key=La clave utilizada no es válida.
verification.token-key=Clave de validación
login.invalid-token=El token de acceso no es válido

descriptor.category=Categoría del descriptor
method.coding-table=Tabla de codificación

oauth-client.info=Información del cliente
oauth-client.list=Lista de clientes de OAuth
client.details.client.id=Detalles del número de identificación del cliente
client.details.additional.info=Información adicional
client.details.token.list=Lista de muestras
client.details.refresh-token.list=Lista de tokens actualizadas
oauth-client.remove=Eliminar
oauth-client.remove.all=Eliminar todo
oauth-client=Cliente
oauth-client.token.issue.date=Fecha de emisión
oauth-client.expires.date=Fecha de expiración
oauth-client.issued.tokens=Tokens emitidos
client.details.add=Añadir cliente OAuth
oauth-client.create=Crear cliente OAuth
oauth-client.id=Número de identificación del cliente
oauth-client.secret=Secreto de cliente
oauth-client.redirect.uri=Redirección URI del cliente
oauth-client.access-token.accessTokenValiditySeconds=Validez del token de acceso
oauth-client.access-token.refreshTokenValiditySeconds=Actualizar validez del token
oauth-client.access-token.defaultDuration=Usar por defecto
oauth-client.title=Título del cliente
oauth-client.description=Descripción
oauth2.error.invalid_client=ID de cliente no válido. Validar los parámetros client_id y client_secret.

team.user.enter.email=Introduzca correo electrónico del usuario
user.not.found=Usuario no encontrado
team.profile.update.title=Actualizar la información del grupo

autocomplete.genus=Encontrar géneros...

stats.number-of-countries={0} Países
stats.number-of-institutes={0} Instituciones
stats.number-of-accessions={0} Accesiones

navigate.back=Atrás


content.page.list.title=Lista de artículos
article.lang=Idioma
article.classPk=Objeto

session.expiry-warning-title=Advertencia de caducidad de la sesión
session.expiry-warning=Su sesión actual está a punto de caducar. ¿Desea continuar con esta sesión?
session.expiry-extend=Extender la sesión

download=Descargar
download.kml=Descargar KML

data-overview=Resumen estadístico
data-overview.intro=Un resumen estadístico de la información guardada en Genesys para los filtros aplicados actualmente; todo, desde el número total de accesiones por país hasta la diversidad de las colecciones.
data-overview.short=Resumen
data-overview.institutes=Instituciones poseedoras
data-overview.composition=Composición de la tenencia del banco de genes
data-overview.sources=Fuentes del material
data-overview.management=Gestión de la colección
data-overview.availability=Disponibilidad del material
data-overview.otherCount=Otros
data-overview.missingCount=No especificado
data-overview.totalCount=Total
data-overview.donorCode=Código WIEWS de la FAO del instituto donante
data-overview.mlsStatus=Disponible para su distribución conforme al Sistema Multilateral (MLS)


admin.cache.page.title=Resumen del caché de la aplicación
cache.stat.map.ownedEntryCount=Entradas de caché de propiedad
cache.stat.map.lockedEntryCount=Entradas de caché cerradas
cache.stat.map.puts=Número de escrituras en caché
cache.stat.map.hits=Número de aciertos de caché

loggers.list.page=Lista de registradores configurados
logger.add-logger=Añadir registrador
logger.edit.page=Página de configuración del registrador
logger.name=Nombre del registrador
logger.log-level=Nivel de registro
logger.appenders=Registro de anexos

menu.admin.index=Administrador
menu.admin.loggers=Registradores
menu.admin.caches=Cachés
menu.admin.ds2=Conjunto de datos DS2

news.content.page.all.title=Lista de noticias
news.archive.title=Archivo de noticias

worldclim.monthly.title=Clima en el sitio de colección
worldclim.monthly.precipitation.title=Precipitación mensual
worldclim.monthly.temperatures.title=Temperaturas mensuales
worldclim.monthly.precipitation=Precipitación mensual [mm]
worldclim.monthly.tempMin=Temperatura mínima [ºC]
worldclim.monthly.tempMean=Temperatura media [ºC]
worldclim.monthly.tempMax=Temperatura máxima [ºC]

worldclim.other-climate=Otros datos climáticos

download.page.title=Antes de descargar
download.download-now=Empezar la descarga.

resolver.page.index.title=Encontrar el identificador permanente
resolver.acceNumb=Número de accesión
resolver.instCode=Código del instituto poseedor
resolver.lookup=Encontrar el identificador
resolver.uuid=UUID
resolver.resolve=Resolver

resolver.page.reverse.title=Resultados de resolución
accession.purl=URL permanente

menu.admin.kpi=KPI
admin.kpi.index.page=KPI
admin.kpi.execution.page=Ejecución de KPI
admin.kpi.executionrun.page=Detalles de la ejecución

accession.pdci=Índice de la integridad de los datos de pasaporte
accession.pdci.jumbo=Puntuación PDCI\: {0,number,0.00} de 10.0
accession.pdci.institute-avg=El promedio de puntuación PDCI para este instituto es de {0,number,0.00}
accession.pdci.about-link=Lea sobre el índice de la integridad de los datos de pasaporte
accession.pdci.independent-items=Independiente del tipo de población
accession.pdci.dependent-items=Dependiente del tipo de población
accession.pdci.stats-text=El promedio de puntuación PDCI para {0} accesiones es de {1,number,0.00}, con una puntuación mínima de {2,number,0.00} y una puntuación máxima de {3,number,0.00}.

accession.donorNumb=Código de la institución donante
accession.acqDate=Fecha de la adquisición

accession.svalbard-data.url=URL de la base de datos Svalbard
accession.svalbard-data.url-title=Colocar información en la base de datos SGSV
accession.svalbard-data.url-text=Ver la información del depósito SGSV para {0}

filter.download-pdci=Descargar datos PDCI
statistics.phenotypic.stats-text=De las {0} accesiones, {1} accesión(es) ({2,number,0.##%}) tiene(n) al menos una característica adicional registrada en un conjunto de datos disponible en Genesys (de media {3,number,0.##} características en {4,number,0.##} conjuntos de datos).

twitter.tweet-this=Tuitear
twitter.follow-X=Seguir a @{0}
linkedin.share-this=Compartir en LinkedIn
share.link=Compartir enlace
share.link.placeholder=Espere, por favor...
share.link.text=Por favor, utilíce la versión corta de la URL completa para esta página\:

welcome.read-more=Lea más acerca de Genesys
twitter.latest-on-twitter=Lo último en Twitter
video.play-video=Reproducir vídeo

heading.general-info=Información general
heading.about=Acerca de
heading.see-also=Ver también
see-also.map=Mapa de localidades de accesión
see-also.overview=Resumen de la colección
see-also.country=Más sobre conservación PGR -Recursos fitogenéticos- en {0}

help.page.intro=Consulte la sección de tutoriales para aprender cómo utilizar Genesys.

charts=Gráficos y Mapas
charts.intro=Aquí encontrará algunos gráficos y mapas de utilidad para su próxima presentación.
chart.collections.title=Tamaño de las recolecciones del banco de genes
chart.attribution-text=www.genesys-pgr.org
chart.collections.series=Número de accesiones en el banco de genes

label.load-more-data=Cargar más...

userlist.list-my-lists=Listas de accesiones guardadas
userlist.title=Título de la lista
userlist.description=Descripción de la lista
userlist.disconnect=Desconectar la lista
userlist.update-list=Guardar la actualización de la lista
userlist.make-new-list=Crear una nueva lista
userlist.shared=Permitir que otros accedan a la lista

region.page.list.title = Información geográfica de la FAO
region.page.list.intro = Las regiones geográficas de FAO enumeradas a continuación le permiten acceder a los datos de accesiones recogidas o mantenidas en la región.
region.page.show.parent = Mostrar región superior {0}
region.page.show.world = Mostrar todas las regiones del mundo
region.countries-in-region=Lista de países en {0}
region.regions-in-region=Regiones de FAO en {0}
region.show-all-regions=Lista de todas las regiones de FAO

