Genesys Backup and Disaster Recovery Manual
===========================================
December 2015: Documentation commit {buildNumber}
:revnumber: {projectVersion}
:doctype: book
:toc: left
:toclevels: 5
:icons: font
:numbered:
:source-highlighter: pygments
:pygments-css: class
:pygments-linenums-mode: table


[[intro]]
Introduction
------------

This manual provides all information required to properly backup Genesys application,
configuration and data for a safe and valid recovery.

[NOTE]
====
This document is written under the assumption you are familiar with basics of GNU/Linux
administration and shell scripting.
====


include::sections/backup.adoc[]
include::sections/recovery.adoc[]


