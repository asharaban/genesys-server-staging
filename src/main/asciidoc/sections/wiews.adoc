[[wiews]]

== FAO WIEWS

The http://www.fao.org/wiews-archive/wiews.jsp[World Information and Early Warning System (WIEWS)]
on Plant Genetic Resources for Food and Agriculture (PGRFA), has been established by FAO, 
as a world-wide dynamic mechanism to foster information exchange among Member Countries and as an instrument for the periodic
assessment of the State of the World's PGRFA.

The FAO WIEWS database contains basic information about institutes working with PGRFA. The data includes
full names, acronyms, website links and contact information. 

Genesys regularly updates the list of institutes from the FAO WIEWS database and makes them accessible
at https://www.genesys-pgr.org/wiews/active. 

NOTE: This data cannot be directly managed through Genesys, changes must be applied to the WIEWS database.

[[wiews-instcode]]
=== WIEWS Institute Codes

The FAO WIEWS code of the institute consist of the 3-letter <<iso-3166,ISO 3166-1 alpha 3>> country code of 
the country where the institute is located plus a number (e.g. COL001, USA1004). 

The Multi-Crop Passport Descriptors standard relies on WIEWS codes. 

The automated import of institute data allows Genesys to present individual pages for 
genebanks registered in FAO WIEWS database.

[cols="1,2", options="header"] 
.Direct access to genebank pages using WIEWS code
|===
|WIEWS Code
|Genesys URL

|COL001
|https://www.genesys-pgr.org/wiews/COL001
|NGA039
|https://www.genesys-pgr.org/wiews/NGA039
|===

=== Obtaining a WIEWS code

Institute codes can be generated on line by national FAO WIEWS administrators (WIEWS@fao.org) or 
can be requested by filling the form at http://www.fao.org/wiews-archive/wiewspage.jsp?show=newuserdialogMCPD.jsp

=== Inactive WIEWS codes

The WIEWS code of an institute may change. In that case, the record is marked as inactive and it
will refer to the newly assigned code. Genesys will render a message that the institute record
is archived and provide a link to the new code:

.https://www.genesys-pgr.org/wiews/ALB017
image::wiews-archived.png[role="text-center"]

