/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.genesys2.server.model.VersionedAuditedModel;

/**
 * CMS MenuItem.
 *
 * @author mobreza
 */
@Entity
@Table(name = "cmsmenuitem")
public class MenuItem extends VersionedAuditedModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2851932999726435674L;

	/**
	 * MenuItems in a {@link Menu} are ordered by orderIndex
	 */
	private float orderIndex = 10f;

	/** Menu item display text, a translation code */
	private String text;

	/** Menu item title, a translation code */
	private String title;

	/** Destination URL */
	private String url;

	/** Target window */
	private String target;

	/** Parent menu **/
	@ManyToOne(optional = false)
	@JoinColumn(name = "menuId")
	private Menu menu;

	public float getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(float orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
}
