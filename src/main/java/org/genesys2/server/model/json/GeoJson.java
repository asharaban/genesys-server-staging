/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

import org.genesys2.server.model.json.Api1Constants.Geo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoJson {
	@JsonProperty(value = Geo.LATITUDE)
	private Double latitude;
	@JsonProperty(value = Geo.LONGITUDE)
	private Double longitude;
	@JsonProperty(value = Geo.ELEVATION)
	private Double elevation;
	@JsonProperty(value = Geo.COORDUNCERT)
	private Double coordUncert;
	@JsonProperty(value = Geo.COORDDATUM)
	private String coordDatum;
	@JsonProperty(value = Geo.GEOREFMETH)
	private String georefMeth;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public Double getCoordUncert() {
		return coordUncert;
	}

	public void setCoordUncert(Double coordUncert) {
		this.coordUncert = coordUncert;
	}

	public String getCoordDatum() {
		return coordDatum;
	}

	public void setCoordDatum(String coordDatum) {
		this.coordDatum = coordDatum;
	}

	public String getGeorefMeth() {
		return georefMeth;
	}

	public void setGeorefMeth(String georefMeth) {
		this.georefMeth = georefMeth;
	}

}
