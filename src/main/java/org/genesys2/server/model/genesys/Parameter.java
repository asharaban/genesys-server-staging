/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.GlobalVersionedAuditedModel;
import org.genesys2.server.model.impl.Crop;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "parameter")
public class Parameter extends GlobalVersionedAuditedModel implements AclAwareModel {

	private static final long serialVersionUID = -4831244149317371274L;

	// private Language language;
	@Column(length = 36, unique = true, nullable = false)
	private String uuid;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "cropId")
	private Crop crop;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "categoryId", nullable = false)
	private ParameterCategory category;

	@Column(nullable = false, length = 32)
	private String title;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String i18n;

	@Transient
	private JsonNode titleJ;

	public Parameter() {
	}

	@PrePersist
	void ensureUUID() {
		if (this.uuid == null) {
			this.uuid = UUID.nameUUIDFromBytes(title.getBytes()).toString();
		}
	}

	public Crop getCrop() {
		return this.crop;
	}

	public void setCrop(final Crop crop) {
		this.crop = crop;
	}

	public ParameterCategory getCategory() {
		return this.category;
	}

	public void setCategory(final ParameterCategory category) {
		this.category = category;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public void setI18n(String i18n) {
		this.i18n = i18n;
	}

	private synchronized void parseTitleLocal() {
		if (this.titleJ == null && this.i18n != null) {
			final ObjectMapper mapper = new ObjectMapper();
			try {
				this.titleJ = mapper.readTree(i18n);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Transient
	private final Map<String, String> titleMap = new HashMap<String, String>();

	/**
	 * Method to return the map of available languages keys
	 *
	 * @return Map<String,String> with language code as the key and the
	 *         vernacular string as the value.
	 */
	public Map<String, String> getLocalTitleMap() {
		return buildLocalTitleMap();
	}

	private synchronized Map<String, String> buildLocalTitleMap() {

		if (this.titleMap.isEmpty() && this.i18n != null) {
			parseTitleLocal();
			final Iterator<String> languages = this.titleJ.fieldNames();
			while (languages.hasNext()) {
				final String language = languages.next();
				titleMap.put(language, this.titleJ.get(language).textValue());
			}
		}
		return titleMap;
	}

	public String getTitle(Locale locale) {
		return getTitleLocal(locale);
	}

	private synchronized String getTitleLocal(Locale locale) {
		parseTitleLocal();
		return this.titleJ != null && this.titleJ.has(locale.getLanguage()) ? this.titleJ.get(locale.getLanguage()).textValue() : this.title;
	}

	// @Override
	// public String toString() {
	// return MessageFormat
	// .format("Parameter id={0,number,#} title={1} lang={2} crop={3} cat={4}",
	// id, title, language.getId(), crop.getName(),
	// category.getTitle());
	// }

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
