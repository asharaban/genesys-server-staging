package org.genesys2.server.model.elastic;

import java.util.Set;

import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Institute {

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String acronym;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String code;
	@Field(type = FieldType.Object)
	private Country country;
	@Field(type = FieldType.String)
	private String fullName;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private Set<String> networks;

	public static Institute from(FaoInstitute institute) {
		if (institute == null)
			return null;
		Institute i = new Institute();
		i.acronym = institute.getAcronym();
		i.code = institute.getCode();
		i.country = Country.from(institute.getCountry());
		i.fullName = institute.getFullName();
		return i;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Set<String> getNetworks() {
		return networks;
	}
	
	public void setNetworks(Set<String> networks) {
		this.networks = networks;
	}
}