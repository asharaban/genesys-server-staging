/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.service.impl.DirectMysqlQuery;
import org.genesys2.server.service.impl.DirectMysqlQuery.MethodResolver;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

@Repository
public class AccessionListRepositoryCustomImpl implements AccessionListCustomRepository {
	public static final Log LOG = LogFactory.getLog(AccessionListRepositoryCustomImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private MethodRepository methodRepository;

	@Override
	public int addAll(AccessionList accessionList, Collection<Long> accessionIds) {
		Query q = entityManager.createNativeQuery("insert ignore into accelistitems (listid, acceid) values (?1, ?2)");
		q.setParameter(1, accessionList);
		int count = 0;
		for (Long acceId : accessionIds) {
			q.setParameter(2, acceId);
			count += q.executeUpdate();
		}
		return count;
	}

	@Override
	public int addAll(AccessionList list, AppliedFilters filter) {
		final DirectMysqlQuery directQuery = new DirectMysqlQuery("accession", "a");
		directQuery.jsonFilter(filter, new MethodResolver() {
			@Override
			public Method getMethod(final long methodId) {
				return AccessionListRepositoryCustomImpl.this.methodRepository.findOne(methodId);
			}
		});
		directQuery.sort(new Sort("acceNumb"));

		Query q = entityManager.createNativeQuery("insert ignore into accelistitems (listid, acceid) (" + directQuery.getQuery("?, a.id") + ")");
		q.setParameter(1, list.getId());

		if (LOG.isDebugEnabled())
			LOG.debug("Updating list: " + list.getUuid() + " id=" + list.getId());

		int param = 2;
		for (Object pv : directQuery.getParameters()) {
			q.setParameter(param++, pv);
		}

		return q.executeUpdate();
	}

	@Override
	public int removeAll(AccessionList accessionList) {
		Query q = entityManager.createNativeQuery("delete from accelistitems where listid = ?1");
		q.setParameter(1, accessionList);
		return q.executeUpdate();
	}
}
