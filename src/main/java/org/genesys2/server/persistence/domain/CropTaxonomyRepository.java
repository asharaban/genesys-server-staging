/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CropTaxonomyRepository extends JpaRepository<CropTaxonomy, Long> {

	/**
	 * Returns all
	 *
	 * @param crop
	 * @return
	 */
	List<CropTaxonomy> findByCrop(Crop crop);

	Page<CropTaxonomy> findByCrop(Crop crop, Pageable pageable);

	@Query("select distinct t from CropTaxonomy ct inner join ct.taxonomy t where ct.crop = ?1")
	List<Taxonomy2> findTaxonomiesByCrop(Crop crop);

	@Query("select distinct ct.crop from CropTaxonomy ct where ct.taxonomy= ?1")
	List<Crop> findCropsByTaxonomy(Taxonomy2 taxonomy);

	@Query("select ct from CropTaxonomy ct where ct.taxonomy= ?1")
	List<CropTaxonomy> findByTaxonomy(Taxonomy2 taxonomy);
}
