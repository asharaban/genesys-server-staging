/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.security.lockout;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * {#link {@link AccountLockoutManager} keeps track of successive failed login
 * attempts and locks the user account if there are more than {
 * {@link #lockAfterXFailures} successive failures.
 *
 * @author Matija Obreza, matija.obreza@croptrust.org
 *
 */
@Component
public class AccountLockoutManager {
	private final Log _log = LogFactory.getLog(getClass());

	private final Map<String, AttemptStatistics> loginAttempts = new HashMap<String, AttemptStatistics>();

	@Autowired
	private UserService userService;

	private int lockAfterXFailures = 5;

	/**
	 * Set number of successive failed login attempts that result in account
	 * lockout
	 *
	 * @param lockAfterXFailures
	 */
	public void setLockAfterXFailures(int lockAfterXFailures) {
		_log.info("Will lock user accounts after " + lockAfterXFailures + " successive failed attempts.");
		this.lockAfterXFailures = lockAfterXFailures;
	}

	/**
	 * Reset failed attempt statistics on successful login
	 *
	 * @param userName
	 */
	synchronized void handleSuccessfulLogin(String userName) {
		purge();
		if (loginAttempts.containsKey(userName)) {
			final AttemptStatistics stats = loginAttempts.get(userName);
			loginAttempts.remove(userName);
			_log.info("Successful login. Removed failed login statistics for " + userName + " " + stats);
		}
	}

	/**
	 * Update failed attempt statistics on failed login
	 *
	 * @param userName
	 */
	synchronized void handleFailedLogin(String userName) {
		purge();

		AttemptStatistics stats = null;
		if (loginAttempts.containsKey(userName)) {
			stats = loginAttempts.get(userName);
		} else {
			final User user = userService.getUserByEmail(userName);
			if (user != null) {
				stats = new AttemptStatistics();
				stats.uuid = user.getUuid();
				loginAttempts.put(userName, stats);
			} else {
				if (_log.isDebugEnabled()) {
					_log.debug("No such user username=" + userName);
				}
			}
		}

		if (stats != null) {
			stats.count++;
			stats.lastAttempt = new Date();
			_log.info("Updated failed login statistics for username=" + userName + " " + stats);

			if (stats.count >= lockAfterXFailures) {
				_log.warn("Too many failed login attempts. Locking account for username=" + userName);
				userService.setAccountLockLocal(stats.uuid, true);
			}
		}

	}

	/**
	 * Removes expired statistics
	 */
	synchronized void purge() {
		if (loginAttempts.size() == 0) {
			return;
		}

		if (_log.isDebugEnabled()) {
			_log.debug("Purging expired entries");
		}

		final List<String> userNames = new ArrayList<String>(loginAttempts.keySet());
		final long now = new Date().getTime();

		for (final String userName : userNames) {
			final AttemptStatistics stats = loginAttempts.get(userName);
			if (stats == null) {
				loginAttempts.remove(userName);
				continue;
			}

			// Things older than 60minutes=60*60*1000
			if (now - stats.lastAttempt.getTime() >= 60 * 60 * 1000) {
				loginAttempts.remove(userName);
				_log.info("Removed expired failed login statistics for " + userName + " " + stats);
			}
		}

		if (_log.isDebugEnabled()) {
			_log.debug("Number of failed login attempts in memory: " + loginAttempts.size());
		}
	}

	private class AttemptStatistics {
		String uuid;
		int count = 0;
		Date lastAttempt = new Date();

		@Override
		public String toString() {
			return "count=" + count + " lastAttempt=" + lastAttempt;
		}
	}

}
