/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;

public interface AccessionListService {

	AccessionList getList(Long id);

	void save(AccessionList accessionList);

	List<AccessionList> getAll();

	void delete(AccessionList accessionList);

	List<AccessionList> getMyLists();

	Set<Long> getAccessionIds(AccessionList accessionList);

	AccessionList getList(UUID uuid);

	List<AccessionList> getLists(List<UUID> uuid);

	void removeAll(AccessionList accessionList);

	void addToList(AccessionList list, AccessionData accession);

	void addToList(AccessionList accessionList, Collection<Long> accessionIds);

	void addToList(AccessionList accessionList, AppliedFilters filters);

	void setList(AccessionList accessionList, Collection<Long> accessionIds);

	int sizeOf(AccessionList accessionList);


}