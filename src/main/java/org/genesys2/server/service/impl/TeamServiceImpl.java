/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.persistence.domain.TeamRepository;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.UserService;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TeamServiceImpl implements TeamService {
	private static final Log LOG = LogFactory.getLog(TeamServiceImpl.class);

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private UserService userService;

	@Override
	@PostAuthorize("hasRole('ADMINISTRATOR') or hasPermission(returnObject, 'READ')")
	public Team getTeam(String uuid) {
		final Team team = teamRepository.findOneByUuid(uuid);
		return team;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("isAuthenticated() and hasRole('VETTEDUSER')")
	public Team addTeam(String name) {
		final User user = SecurityContextUtil.getCurrentUser();

		final Team team = new Team();
		team.setName(name);

		final Set<User> member1 = new HashSet<User>();
		member1.add(user);
		team.setMembers(member1);

		teamRepository.save(team);
		LOG.info("New team created " + team);
		return team;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#team, 'CREATE')")
	public Team addTeamMember(String teamUuid, User user) {
		final Team team = teamRepository.findOneByUuid(teamUuid);
		if (team.getMembers().contains(user)) {
			LOG.info("User already member of this team");
			return team;
		}

		team.getMembers().add(user);
		teamRepository.save(team);
		// TODO Add permissions
		LOG.info("Added user to " + team);
		return team;
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#team, 'CREATE')")
	@Transactional(readOnly = false)
	public Team removeTeamMember(String teamUuid, User user) {
		final Team team = teamRepository.findOneByUuid(teamUuid);
		if (team.getMembers().remove(user)) {
			teamRepository.save(team);
			LOG.info("Removed " + user + " from " + team);
			// TODO Revoke permissions
		}
		LOG.warn("Could not remove user from team.");

		return team;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("isAuthenticated()")
	public void removeMe(long teamId) {
		removeMe(teamRepository.findOne(teamId));
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("isAuthenticated()")
	public void removeMe(Team team) {
		final User user = SecurityContextUtil.getCurrentUser();
		final boolean removed = team.getMembers().remove(user);
		if (removed) {
			teamRepository.save(team);
			LOG.info("Removed " + user + " from " + team);
			// TODO Revoke permissions
		}
		LOG.warn("Could not remove user from team.");
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public Team addTeamInstitute(Team team, FaoInstitute institute) {
		if (team.getInstitutes().contains(institute)) {
			LOG.info("Institute already assigned to this team");
		} else {
			team.getInstitutes().add(institute);
			teamRepository.save(team);
			LOG.info("Added teams to " + team);
		}

		return team;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public Team removeTeamInsitute(Team team, FaoInstitute institute) {
		if (team.getInstitutes().contains(institute)) {
			team.getInstitutes().remove(institute);
			teamRepository.save(team);
			LOG.info("Removed institute from " + team);
		}
		return team;
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public List<Team> listMyTeams() {
		final User user = SecurityContextUtil.getCurrentUser();
		return listUserTeams(user);
	}

	@Override
	public List<Team> listUserTeams(User user) {
		return teamRepository.listForUser(user);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public Page<Team> listTeams(Pageable pageable) {
		return teamRepository.findAll(pageable);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#team, 'READ')")
	public List<User> getMembers(Team team) {
		return teamRepository.listMembers(team);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#team, 'WRITE')")
	public void updateTeamInformation(String teamUuid, String teamName) {
		final Team team = teamRepository.findOneByUuid(teamUuid);
		team.setName(teamName);
		teamRepository.saveAndFlush(team);
	}
}
