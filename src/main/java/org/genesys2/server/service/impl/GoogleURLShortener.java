/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import org.genesys2.server.service.UrlShortenerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.social.support.ClientHttpRequestFactorySelector;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.*;

@Component
@SuppressWarnings("unchecked")
public class GoogleURLShortener implements UrlShortenerService {

    @Value("${google.api.key}")
    private String apiKey;

    @Value("${google.url.shortener}")
    private String googleUrlShortener;

    private RestTemplate restTemplate;

    public GoogleURLShortener() {
        restTemplate = new RestTemplate(ClientHttpRequestFactorySelector.getRequestFactory());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
    }

    @Override
    public String shortenUrl(URL url) {

        Map<String, String> request = new HashMap<String, String>();
        request.put("longUrl", url.toString());
        LinkedHashMap<String, String> shortUrl = restTemplate.postForObject(googleUrlShortener+"?key="+apiKey, request, LinkedHashMap.class);

        return shortUrl.get("id");
    }

}
