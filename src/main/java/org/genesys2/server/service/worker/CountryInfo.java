/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.io.Serializable;
import java.text.MessageFormat;

public class CountryInfo implements Serializable {
	private static final long serialVersionUID = 518678720881886517L;
	private final String iso, iso3, isoNum;
	private final String name;
	private final boolean active;
	private final Long refnameId;

	public CountryInfo(final String iso, final String iso3, final String isoNum, final String name, final boolean active) {
		this.iso = iso;
		this.iso3 = iso3;
		this.isoNum = isoNum;
		this.name = name;
		this.active = active;
		this.refnameId = null;
	}

	public CountryInfo(final String iso, final String iso3, final String isoNum, final String name, final Long refnameId, final boolean active) {
		this.iso = iso;
		this.iso3 = iso3;
		this.isoNum = isoNum;
		this.name = name;
		this.active = active;
		this.refnameId = refnameId;
	}

	public String getIso() {
		return iso;
	}

	public String getIso3() {
		return iso3;
	}

	public String getIsoNum() {
		return isoNum;
	}

	public String getName() {
		return name;
	}

	public boolean isActive() {
		return active;
	}

	public Long getRefnameId() {
		return refnameId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (iso == null ? 0 : iso.hashCode());
		result = prime * result + (iso3 == null ? 0 : iso3.hashCode());
		result = prime * result + (isoNum == null ? 0 : isoNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final CountryInfo other = (CountryInfo) obj;
		if (iso == null) {
			if (other.iso != null) {
				return false;
			}
		} else if (!iso.equals(other.iso)) {
			return false;
		}
		if (iso3 == null) {
			if (other.iso3 != null) {
				return false;
			}
		} else if (!iso3.equals(other.iso3)) {
			return false;
		}
		if (isoNum == null) {
			if (other.isoNum != null) {
				return false;
			}
		} else if (!isoNum.equals(other.isoNum)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Country iso={0} iso3={1} name={2} active={3}", iso, iso3, name, active);
	}
}
