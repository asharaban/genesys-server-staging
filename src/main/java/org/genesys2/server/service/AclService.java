/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;
import java.util.Map;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.acl.AclEntry;
import org.genesys2.server.model.acl.AclObjectIdentity;
import org.genesys2.server.model.acl.AclSid;
import org.genesys2.server.security.AuthUserDetails;
import org.springframework.security.acls.model.Permission;

public interface AclService {

	void addCreatorPermissions(AclAwareModel target);

	/**
	 * List ObjectIdentities of specified class for user with specified
	 * permission
	 *
	 * @param clazz
	 * @param authUser
	 * @return
	 */
	List<Long> listIdentitiesForSid(Class<? extends AclAwareModel> clazz, AuthUserDetails authUser, Permission permission);

	AclObjectIdentity getObjectIdentity(long id);

	AclObjectIdentity getObjectIdentity(String clazz, long id);

	AclObjectIdentity ensureObjectIdentity(String clazz, long id);

	AclObjectIdentity getObjectIdentity(AclAwareModel entity);

	List<AclEntry> getAclEntries(AclObjectIdentity objectIdentity);

	List<AclEntry> getAclEntries(AclAwareModel entity);

	Permission[] getAvailablePermissions(String className);

	List<AclSid> getSids(AclAwareModel entity);

	List<AclSid> getSids(long id, String className);

	Map<String, Map<Integer, Boolean>> getPermissions(AclAwareModel entity);

	Map<String, Map<Integer, Boolean>> getPermissions(long id, String className);

	boolean addPermissions(long objectIdIdentity, String className, String uuid, boolean principal, Map<Integer, Boolean> permissions);

	void updatePermission(AclObjectIdentity entity, String sid, Map<Integer, Boolean> permissionMap);

	List<AclSid> getAllSids();
}
