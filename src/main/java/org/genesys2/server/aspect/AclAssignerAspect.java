/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.service.AclService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AclAssignerAspect {

	private static final Log LOG = LogFactory.getLog(AclAssignerAspect.class);

	@Autowired
	private AclService aclService;

	@Around("execution(* org.genesys2.server.persistence.domain.*.save(..))")
	public Object aroundSaveAclObject(ProceedingJoinPoint pjp) throws Throwable {
		final Object arg0 = pjp.getArgs()[0];
		boolean needsAcl = false;

		if (arg0 instanceof AclAwareModel) {
			final AclAwareModel aclModel = (AclAwareModel) arg0;
			needsAcl = aclModel.getId() == null;
		}

		try {
			final Object retval = pjp.proceed();
			if (needsAcl) {
				final AclAwareModel aclModel = (AclAwareModel) retval;
				LOG.warn("Inserting ACL entries for owner: " + aclModel.getId());
				aclService.addCreatorPermissions(aclModel);
			}

			return retval;
		} finally {
			// Nothing to do here
		}
	}

}
