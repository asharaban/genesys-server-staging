/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component("envVarFilter")
public class EnvVariablesFilter extends OncePerRequestFilter {

	@Value("${google.analytics.account}")
	private String googleAnalyticsAccount;

	@Value("${cdn.flags.url}")
	private String cdnFlagsUrl;

	@Override
	public void afterPropertiesSet() throws ServletException {
		super.afterPropertiesSet();
		cdnFlagsUrl = StringUtils.defaultIfBlank(cdnFlagsUrl, null);
		googleAnalyticsAccount = StringUtils.defaultIfBlank(googleAnalyticsAccount, null);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

		if (googleAnalyticsAccount != null) {
			request.setAttribute("googleAnalyticsAccount", googleAnalyticsAccount);
		}

		if (cdnFlagsUrl != null) {
			request.setAttribute("cdnFlagsUrl", cdnFlagsUrl);
		}

		filterChain.doFilter(request, response);
	}

}
