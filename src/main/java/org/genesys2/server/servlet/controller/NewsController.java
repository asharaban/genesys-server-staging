/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.ArrayList;
import java.util.List;

import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.model.impl.Menu;
import org.genesys2.server.model.impl.MenuItem;
import org.genesys2.server.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class NewsController extends BaseController {

    @Value("${news.menu.abbreviate.length}")
    private int abrLength;

    @Autowired
    private ContentService contentService;

    @Autowired
    private JspHelper jspHelper;

    @RequestMapping(value = "/content/news", method = RequestMethod.GET)
    public String getAllNews(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page) {

        Page<ActivityPost> allNews = contentService.allNews(page);

        model.addAttribute("pagedData", allNews);

        return "/content/news-index";
    }


    @RequestMapping(value = "/content/news/{id:.+}/{title:.+} ", method = RequestMethod.GET)
    public String getNewsById(@PathVariable("id") Long id, Model model) throws Exception {

        ActivityPost activityPost = contentService.getActivityPost(id);

        List<MenuItem> menuItems = new ArrayList<MenuItem>();

        MenuItem archiveItem = new MenuItem();
        archiveItem.setText(messageSource.getMessage("news.archive.title", new Object[]{}, LocaleContextHolder.getLocale()));
        archiveItem.setUrl("/content/news");
        menuItems.add(archiveItem);

        List<ActivityPost> posts = contentService.lastNews();

        for (ActivityPost post : posts) {

            if (post.getId().equals(id)) continue;

            MenuItem item = new MenuItem();
            item.setText(jspHelper.htmlToText(post.getTitle(), abrLength));
            item.setUrl("/content/news/" + post.getId() + "/" + jspHelper.suggestUrlForText(post.getTitle()));
            menuItems.add(item);
        }

        Menu menu = new Menu();
        menu.setKey("temp menu");
        menu.setItems(menuItems);

        model.addAttribute("news", activityPost);
        model.addAttribute("menu", menu);

        return "/content/news";
    }

    @RequestMapping(value = "/content/news/edit/{id:.+}", method = RequestMethod.GET)
    public String editNews(@PathVariable("id") Long id, Model model) throws Exception {

        ActivityPost activityPost = contentService.getActivityPost(id);
        model.addAttribute("news", activityPost);

        return "/content/news";
    }
}

