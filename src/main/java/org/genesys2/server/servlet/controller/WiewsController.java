/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.DownloadService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.StatisticsService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.SearchException;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Scope("request")
@RequestMapping("/wiews")
public class WiewsController extends BaseController {

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private GeoService geoService;

	@Autowired
	private DownloadService downloadService;

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private ElasticService elasticService;

	@RequestMapping("/")
	public String view(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("pagedData", instituteService.listPGRInstitutes(new PageRequest(page - 1, 50, new Sort("code"))));
		return "/wiews/index";
	}

	@RequestMapping("/active")
	public String viewGenesys(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("activeOnly", true);
		model.addAttribute("pagedData", instituteService.listActive(new PageRequest(page - 1, 50, new Sort("code"))));
		return "/wiews/index";
	}

	@RequestMapping("/active/map")
	public String viewGenesysMap(ModelMap model) {
		final Page<FaoInstitute> institutes = instituteService.listActive(new PageRequest(0, 1000));
		model.addAttribute("jsonInstitutes", geoService.toJson(institutes.getContent()).toString());
		return "/wiews/map";
	}

	@RequestMapping("/{wiewsCode}")
	public String view(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode) {
		_logger.debug("Viewing institute " + wiewsCode);

		if (!wiewsCode.toUpperCase().equals(wiewsCode)) {
			return "redirect:/wiews/" + wiewsCode.toUpperCase();
		}

		final FaoInstitute faoInstitute = instituteService.findInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("faoInstitute", faoInstitute);
		model.addAttribute("organizations", organizationService.getOrganizations(faoInstitute));

		model.addAttribute("blurp", contentService.getArticle(faoInstitute, "blurp", getLocale()));

		model.addAttribute("countByInstitute", genesysService.countByInstitute(faoInstitute));
		long datasetCount = genesysService.countDatasets(faoInstitute);
		model.addAttribute("datasetCount", datasetCount);

		{
			AppliedFilters instituteFilter = new AppliedFilters();
			instituteFilter.add(new AppliedFilter().setFilterName(FilterConstants.INSTCODE).addFilterValue(
					new FilterHandler.LiteralValueFilter(faoInstitute.getCode())));

			try {
				model.addAttribute("statisticsCrops", elasticService.termStatisticsAuto(instituteFilter, FilterConstants.CROPS, 5));
			} catch (SearchException e) {
				_logger.warn(e.getMessage());
			}
		}
		model.addAttribute("statisticsGenus", genesysService.statisticsGenusByInstitute(faoInstitute, new PageRequest(0, 5)));
		model.addAttribute("statisticsTaxonomy", genesysService.statisticsSpeciesByInstitute(faoInstitute, new PageRequest(0, 5)));
		model.addAttribute("statisticsPDCI", statisticsService.statisticsPDCI(faoInstitute));

		if (datasetCount > 0) {
			model.addAttribute("statisticsPheno", statisticsService.statisticsPheno(faoInstitute));
		}

		return "/wiews/details";
	}

	@RequestMapping(value = "/{wiewsCode}/stat-genus", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Object[]> getGenusStats(@PathVariable(value = "wiewsCode") String wiewsCode) {
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		return genesysService.statisticsGenusByInstitute(faoInstitute, new PageRequest(0, 10)).getContent();
	}

	@RequestMapping(value = "/{wiewsCode}/stat-species", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Object[]> getSpeciesStats(@PathVariable(value = "wiewsCode") String wiewsCode) {
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		return genesysService.statisticsSpeciesByInstitute(faoInstitute, new PageRequest(0, 20)).getContent();
	}

	@RequestMapping("/{wiewsCode}/datasets")
	public String viewDatasets(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, @RequestParam(value = "page", defaultValue = "1") int page) {
		_logger.debug("Viewing datasets for instCode=" + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("faoInstitute", faoInstitute);
		model.addAttribute("pagedData", genesysService.listDatasets(faoInstitute, new PageRequest(page - 1, 30)));

		return "/metadata/index";
	}

	@RequestMapping("/{wiewsCode}/edit")
	public String edit(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode) {
		view(model, wiewsCode);
		return "/wiews/edit";
	}

	@RequestMapping("/{wiewsCode}/update")
	public String update(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, @RequestParam("blurp") String aboutBody,
			@RequestParam(value = "summary", required = false) String summary, @RequestParam("gaTracker") String gaTracker,
			@RequestParam("mailto") String mailto, @RequestParam("uniqueAcceNumbs") boolean uniqueAcceNumbs,
			@RequestParam(value = "allowMaterialRequests", required = false, defaultValue = "false") boolean allowMaterialRequests,
			@RequestParam("codeSVGS") String codeSVGS) {

		_logger.debug("Updating institite " + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		instituteService.updateAbout(faoInstitute, aboutBody, summary, getLocale());
		final Map<String, String> settings = new HashMap<String, String>();
		settings.put("googleAnalytics.tracker", gaTracker);
		settings.put("requests.mailto", mailto);
		instituteService.updateSettings(faoInstitute, settings);
		instituteService.setUniqueAcceNumbs(faoInstitute, uniqueAcceNumbs);
		instituteService.setAllowMaterialRequests(faoInstitute, allowMaterialRequests);
		instituteService.setCodeSGSV(faoInstitute, codeSVGS);

		return "redirect:/wiews/" + wiewsCode;
	}

	@RequestMapping("/{wiewsCode}/data")
	public String viewData(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing country " + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("filter", "{\"" + FilterConstants.INSTCODE + "\":[\"" + faoInstitute.getCode() + "\"]}");
		model.addAttribute("page", page);
		return "redirect:/explore";
	}
	
	@RequestMapping("/{wiewsCode}/data/map")
	public String viewData(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode) {
		_logger.debug("Viewing map for institute " + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("filter", "{\"" + FilterConstants.INSTCODE + "\":[\"" + faoInstitute.getCode() + "\"]}");
		return "redirect:/explore/map";
	}

	@RequestMapping("/{wiewsCode}/overview")
	public String overview(HttpServletRequest request, @PathVariable(value = "wiewsCode") String wiewsCode) throws UnsupportedEncodingException {
		_logger.debug("Viewing institute overview " + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		AppliedFilters appliedFilters = new AppliedFilters();
		appliedFilters.add(new AppliedFilter().setFilterName(FilterConstants.INSTCODE).addFilterValue(
				new FilterHandler.LiteralValueFilter(faoInstitute.getCode())));

		return "forward:/explore/overview?filter=" + URLEncoder.encode(appliedFilters.toString(), "UTF8");
	}

	@RequestMapping("/{wiewsCode}/t/{genus}")
	public String viewDataByGenusSpecies(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, @PathVariable(value = "genus") String genus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing country " + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		// Taxonomy
		final Taxonomy2 taxonomy2 = taxonomyService.get(genus);
		if (taxonomy2 == null) {
			throw new ResourceNotFoundException();
		}
		_logger.debug("Got " + taxonomy2);

		model.addAttribute("filter", "{\"" + FilterConstants.INSTCODE + "\":[\"" + faoInstitute.getCode() + "\"],\"" + FilterConstants.TAXONOMY_GENUS
				+ "\":[\"" + taxonomy2.getGenus() + "\"]}");
		model.addAttribute("page", page);
		return "redirect:/explore";
	}

	@RequestMapping("/{wiewsCode}/t/{genus}/{species:.+}")
	public String viewDataByGenusSpecies(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, @PathVariable(value = "genus") String genus,
			@PathVariable(value = "species") String species, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing country " + wiewsCode);
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}

		// Taxonomy
		final Taxonomy2 taxonomy2 = taxonomyService.get(genus, species);
		if (taxonomy2 == null) {
			throw new ResourceNotFoundException();
		}
		_logger.debug("Got " + taxonomy2);

		model.addAttribute("filter", "{\"" + FilterConstants.INSTCODE + "\":[\"" + faoInstitute.getCode() + "\"],\"" + FilterConstants.TAXONOMY_GENUS
				+ "\":[\"" + taxonomy2.getGenus() + "\"],\"" + FilterConstants.TAXONOMY_SPECIES + "\":[\"" + taxonomy2.getSpecies() + "\"]}");
		model.addAttribute("page", page);
		return "redirect:/explore";
	}

	@RequestMapping(value = "/{wiewsCode}/download", method = RequestMethod.POST, params = { "dwca" })
	public void downloadDwca(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, HttpServletResponse response) throws IOException {
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		_logger.warn("Searching accessions of: " + faoInstitute);

		// Create JSON filter
		final AppliedFilters appliedFilters = new AppliedFilters();
		final AppliedFilter arr = new AppliedFilter().setFilterName(FilterConstants.INSTCODE);
		arr.addFilterValue(new FilterHandler.LiteralValueFilter(faoInstitute.getCode()));
		appliedFilters.add(arr);

		// Write Darwin Core Archive to the stream.
		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-%1$s.zip\"", faoInstitute.getCode()));

		final OutputStream outputStream = response.getOutputStream();
		genesysService.writeAccessions(appliedFilters, outputStream);
		response.flushBuffer();
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{wiewsCode}/download", method = RequestMethod.POST, params = { "mcpd" })
	public void downloadMcpd(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, HttpServletResponse response) throws IOException {
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		_logger.warn("Searching accessions of: " + faoInstitute);

		// Create JSON filter
		final AppliedFilters appliedFilters = new AppliedFilters();
		final AppliedFilter arr = new AppliedFilter().setFilterName(FilterConstants.INSTCODE);
		arr.addFilterValue(new FilterHandler.LiteralValueFilter(faoInstitute.getCode()));
		appliedFilters.add(arr);

		// Write MCPD to the stream.
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-%1$s.xlsx\"", faoInstitute.getCode()));
		response.flushBuffer();

		final OutputStream outputStream = response.getOutputStream();
		try {
			downloadService.writeXlsxMCPD(appliedFilters, outputStream);
			response.flushBuffer();
		} catch (EOFException e) {
			_logger.warn("Download was aborted", e);
		}
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{wiewsCode}/download", method = RequestMethod.POST, params = { "pdci" })
	public void downloadPdci(ModelMap model, @PathVariable(value = "wiewsCode") String wiewsCode, HttpServletResponse response) throws IOException {
		final FaoInstitute faoInstitute = instituteService.getInstitute(wiewsCode);
		if (faoInstitute == null) {
			throw new ResourceNotFoundException();
		}
		_logger.warn("Searching accessions of: " + faoInstitute);

		// Create JSON filter
		final AppliedFilters appliedFilters = new AppliedFilters();
		final AppliedFilter arr = new AppliedFilter().setFilterName(FilterConstants.INSTCODE);
		arr.addFilterValue(new FilterHandler.LiteralValueFilter(faoInstitute.getCode()));
		appliedFilters.add(arr);

		// Write MCPD to the stream.
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-pdci-%1$s.xlsx\"", faoInstitute.getCode()));
		response.flushBuffer();

		final OutputStream outputStream = response.getOutputStream();
		try {
			downloadService.writeXlsxPDCI(appliedFilters, outputStream);
			response.flushBuffer();
		} catch (EOFException e) {
			_logger.warn("Download was aborted", e);
		}
	}
}
