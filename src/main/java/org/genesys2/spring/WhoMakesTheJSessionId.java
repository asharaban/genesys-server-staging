/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Enable this {@link WebListener} to figure out what initates the HTTP Session.
 * 
 * <ul>
 * <li>init.jsp: session="false"</li>
 * <li>_csrf.getToken() then creates the session anyway in decorators -- this is
 * used for AJAX requests</li>
 * </ul>
 */
// @WebListener
public class WhoMakesTheJSessionId implements HttpSessionListener {
	public WhoMakesTheJSessionId() {
		System.err.println("WhoMakesTheJSessionId listener instantiated.");
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		throw new UnsupportedOperationException("Sessions are not allowed");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		throw new UnsupportedOperationException("Sessions are not allowed");
	}
}