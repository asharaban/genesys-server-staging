package org.genesys2.server.service.worker;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;

import javax.annotation.Resource;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hazelcast.config.Config;
import com.hazelcast.config.ExecutorConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.QueueConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Cluster;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceNotActiveException;
import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;
import com.hazelcast.core.ManagedContext;
import com.hazelcast.core.Member;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import com.hazelcast.spring.context.SpringAware;
import com.hazelcast.spring.context.SpringManagedContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HazelcastDistributedExecutorTest.TestConfig.class, initializers = PropertyPlacholderInitializer.class)
public class HazelcastDistributedExecutorTest {

	private static final Log LOG = LogFactory.getLog(ElasticUpdaterTest.class);

	@EnableAspectJAutoProxy
	@Import({})
	// @ComponentScan(basePackages = {})
	public static class TestConfig {
		@Value("${hazelcast.port}")
		int hazelPort;
		@Value("${hazelcast.instanceName}")
		private String instanceName = "genesys";
		@Value("${hazelcast.password}")
		private String password;
		@Value("${hazelcast.name}")
		private String name;

		@Bean
		public ManagedContext managedContext() {
			return new SpringManagedContext();
		}

		@Bean
		public HazelcastInstance hazelcast(ManagedContext managedContext) {
			Config cfg = new Config();
			cfg.setManagedContext(managedContext);
			cfg.setInstanceName(instanceName);

			GroupConfig groupConfig = cfg.getGroupConfig();
			groupConfig.setName(name);
			groupConfig.setPassword(password);

			cfg.setProperty("hazelcast.merge.first.run.delay.seconds", "5");
			cfg.setProperty("hazelcast.merge.next.run.delay.seconds", "5");
			cfg.setProperty("hazelcast.logging.type", "log4j");
			cfg.setProperty("hazelcast.icmp.enabled", "true");

			NetworkConfig network = cfg.getNetworkConfig();
			network.setPort(hazelPort);
			network.setPortAutoIncrement(false);

			JoinConfig join = network.getJoin();
			join.getMulticastConfig().setEnabled(false);
			TcpIpConfig tcpIpConfig = join.getTcpIpConfig();
			tcpIpConfig.setEnabled(true);
			tcpIpConfig.setConnectionTimeoutSeconds(20);

			// MapConfig mapconfig=new MapConfig();

			ExecutorConfig execConfig = new ExecutorConfig();
			execConfig.setName("hazel-exec");
			execConfig.setPoolSize(4);
			execConfig.setQueueCapacity(2);
			execConfig.setStatisticsEnabled(true);
			cfg.addExecutorConfig(execConfig);

			QueueConfig queueConfig = new QueueConfig();
			queueConfig.setName("elasticsearchQueue");
			queueConfig.setMaxSize(100);
			cfg.addQueueConfig(queueConfig);

			HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);
			return instance;
		}

		@Bean
		public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
			HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
			return cm;
		}

		@Bean
		public IMap<Object, Object> tileserverMap(HazelcastInstance hazelcast) {
			IMap<Object, Object> x = hazelcast.getMap("tileserver");
			return x;
		}

		@Bean
		public IExecutorService distributedExecutor(HazelcastInstance hazelcast) {
			IExecutorService executorService = hazelcast.getExecutorService("hazel-exec");
			return executorService;
		}

		@Bean
		public IQueue<Object> elasticsearchQueue(HazelcastInstance hazelcast) {
			IQueue<Object> elasticsearchQueue = hazelcast.getQueue("elasticsearchQueue");
			return elasticsearchQueue;
		}

		@Bean
		public ListenerFoo listenerFoo() {
			return new ListenerFoo("Foo1");
		}

		@Bean
		public ListenerFoo listenerFoo2() {
			return new ListenerFoo("Foo2");
		}

	}

	@Autowired
	HazelcastInstance hazelcast;

	@Autowired
	HazelcastCacheManager cacheManager;

	@Resource
	IMap<String, Object> tileserverMap;

	@Resource
	transient IQueue<Long> elasticsearchQueue;

	@Resource
	IExecutorService executorService;

	// @Autowired
	// ListenerFoo listenerFoo;

	@Test
	public void test1() {
		LOG.info("Name: " + hazelcast.getName());
		Cluster cluster = hazelcast.getCluster();
		for (Member member : cluster.getMembers()) {
			LOG.info("Cluster member: " + member + " local=" + member.localMember());
		}
		IMap<Object, Object> map = hazelcast.getMap("default");
		LOG.info("Map: " + map);
		IMap<String, Object> tsm = hazelcast.getMap("tileserver");
		LOG.info("Map: " + tsm);
		tsm.put("Test1", "Test1");
		LOG.info("Same? " + (tsm == tileserverMap));
		System.err.println(tileserverMap.get("Test1"));
	}

	@Test
	public void testExe() throws InterruptedException, ExecutionException {
		tileserverMap.put("Test1", "Test1");
		for (int i = 0; i < 500; i++) {
			elasticsearchQueue.add((long) i);
		}

		Set<Future<String>> set = new HashSet<Future<String>>();
		for (int i = 0; i < 10; i++) {
			try {
				Future<String> task = executorService.submit(new Foobar("exec" + i));
				set.add(task);
				LOG.info("Submitted task " + i);
			} catch (RejectedExecutionException e) {
				LOG.error(e.getMessage());
			}
		}

		Thread.sleep(5000);
	}

	@Test
	public void testExe2() throws InterruptedException, ExecutionException {
		for (int i = 0; i < 200; i++) {
			if (!elasticsearchQueue.offer((long) i)) {
				// Queue full?
				Thread.sleep(10);
			}
		}
		Thread.sleep(1000);
		// for (int i = 0; i < 300; i++) {
		// elasticsearchQueue.add((long) i);
		// }
		// Thread.sleep(1000);
		// for (int i = 0; i < 300; i++) {
		// elasticsearchQueue.add((long) i);
		// }
	}

	@Test
	public void testExe3() throws InterruptedException, ExecutionException {

	}

	@SpringAware
	static class Foobar implements Callable<String>, Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8635615920157309873L;
		@Resource
		transient IMap<String, Object> tileserverMap;
		@Resource
		transient IQueue<Long> elasticsearchQueue;
		transient String myName;

		public Foobar(String name) {
			myName = name;
		}

		@Override
		public String call() throws Exception {
			System.err.println("setmyname" + myName);
			System.err.println("mapserver= " + tileserverMap);
			Long item = null;
			do {
				item = elasticsearchQueue.poll();
				if (item == null)
					break;
				System.err.println(myName + " item: " + item);
				Thread.sleep(10 + RandomUtils.nextInt(10));
			} while (item != null);
			Thread.sleep(100 + RandomUtils.nextInt(100));
			return (String) tileserverMap.get("Test1");
		}
	}

	static class ListenerFoo implements Runnable, InitializingBean, DisposableBean {
		@Resource
		transient IQueue<Long> elasticsearchQueue;

		private Thread worker;
		private boolean running;

		private String name;

		public ListenerFoo(String string) {
			this.name = string;
		}

		@Override
		public void run() {
			while (running) {
				try {
					Long item = elasticsearchQueue.poll();

					try {
						if (item != null) {
							LOG.debug(name + " Item: " + item);
							Thread.sleep(RandomUtils.nextInt(10));
						} else {
							// Thread.sleep(100 + RandomUtils.nextInt(100));
						}
					} catch (InterruptedException e) {
					}
				} catch (HazelcastInstanceNotActiveException e) {
					LOG.debug("Hazelcast not active.");
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {
					}
				}
			}
		}

		@Override
		public void afterPropertiesSet() throws Exception {
			this.running = true;
			this.worker = new Thread(this, "hazel-worker-" + name);
			this.worker.start();
		}

		@Override
		public void destroy() throws Exception {
			this.running = false;
			this.worker.interrupt();
		}

	}
}
