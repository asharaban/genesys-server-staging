/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import java.io.File;

import org.jamel.dbf.processor.DbfProcessor;
import org.jamel.dbf.processor.DbfRowProcessor;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

/**
 * Test DBF reader of GRIN Taxonomy data from http://www.ars-grin.gov/misc/tax/
 *
 * @author matijaobreza
 */
@Ignore
public class GRINTaxonomyTest {

	@Value("${download.files.dir}")
	private final String filesDir = "./data";

	// @Test
	public void dbInfoTest() {
		final String dbfInfo = DbfProcessor.readDbfInfo(new File("genusno.dbf"));
		System.out.println(dbfInfo);
	}

	/**
	 * Common names
	 */
	// @Test
	public void testCommon() {
		final File dbf = new File("common.dbf");
		final String dbfInfo = DbfProcessor.readDbfInfo(dbf);
		System.out.println(dbfInfo);

		DbfProcessor.processDbf(dbf, new DbfRowProcessor() {
			@Override
			public void processRow(Object[] row) {
				System.out.print(new String((byte[]) row[0]).trim());
				System.out.print(", ");
				System.out.print(((Number) row[1]).longValue());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[2]).trim());
				System.out.println();
			}
		});
	}

	@Test
	public void testSpecies() {
		final File dbf = new File(filesDir, "species 2.dbf");
		final String dbfInfo = DbfProcessor.readDbfInfo(dbf);
		System.out.println(dbfInfo);

		DbfProcessor.processDbf(dbf, new DbfRowProcessor() {
			@Override
			public void processRow(Object[] row) {
				// if (! "Zea".equals(new String((byte[]) row[3]).trim())) {
				// return;
				// }
				// if ("".equals(new String((byte[]) row[4]).trim()) ||
				// "".equals(new String((byte[]) row[8]).trim()) ) {
				// return;
				// }
				System.out.print(((Number) row[0]).longValue());
				System.out.print(", ");
				System.out.print(((Number) row[1]).longValue());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[3]).trim());
				System.out.print(", 4=");
				System.out.print(new String((byte[]) row[4]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[5]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[6]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[7]).trim());
				System.out.print(", SUB >> ");
				System.out.print(new String((byte[]) row[8]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[9]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[10]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[11]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[12]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[13]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[14]).trim());
				System.out.print(", ");

				System.out.print(new String((byte[]) row[15]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[16]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[17]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[18]).trim());
				System.out.print(", ");

				System.out.print(new String((byte[]) row[20]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[21]).trim());
				System.out.print(", ");

				System.out.print(row[24]);
				System.out.print(", ");
				System.out.print(row[25]);
				System.out.println();
			}
		});
	}

	// @Test
	public void testGenusno() {
		final File dbf = new File("genusno.dbf");
		final String dbfInfo = DbfProcessor.readDbfInfo(dbf);
		System.out.println(dbfInfo);

		DbfProcessor.processDbf(dbf, new DbfRowProcessor() {
			@Override
			public void processRow(Object[] row) {
				System.out.print(new String((byte[]) row[0]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[1]).trim());
				System.out.print(", ");
				System.out.print(((Number) row[2]).longValue());
				System.out.print(", ");
				System.out.print(((Number) row[3]).longValue());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[4]).trim());
				System.out.println();
			}
		});
	}

	// @Test
	public void testTaxonno() {
		final File dbf = new File("taxonno.dbf");
		final String dbfInfo = DbfProcessor.readDbfInfo(dbf);
		System.out.println(dbfInfo);

		DbfProcessor.processDbf(dbf, new DbfRowProcessor() {
			@Override
			public void processRow(Object[] row) {
				System.out.print(new String((byte[]) row[0]).trim());
				System.out.print(", ");
				System.out.print(new String((byte[]) row[1]).trim());
				System.out.print(", ");
				System.out.print(((Number) row[2]).longValue());
				System.out.print(", ");
				System.out.print(((Number) row[3]).longValue());
				System.out.println();
			}
		});
	}
}
